angular.module('starter.services', [])

.service('userService', function() {
  this.userData = {};

  this.user = function() {
        return this.userData;
  };

  this.setId = function(id) {
        this.userData.id = id;
  };
  this.getId = function() {
        return this.userData.id;
  };

  this.setFirstname = function(first_name) {
        this.userData.first_name = first_name;
  };
  this.getFirstname = function() {
        return this.userData.first_name;
  };

  this.setLastname = function(last_name) {
        this.userData.last_name = last_name;
  };
  this.getLastname = function() {
        return this.userData.last_name;
  };

  this.setEmail = function(email) {
        this.userData.email = email;
  };
  this.getEmail = function() {
        return this.userData.email;
  };

  this.setIdRol = function(id_rol) {
        this.userData.id_rol = id_rol;
  };
  this.getIdRol = function() {
        return this.userData.id_rol;
  };

  this.setPicture = function(picture) {
        this.userData.picture = picture;
  };
  this.getPicture = function() {
        return this.userData.picture;
  };

  this.setSound = function(sound) {
        this.userData.sound = sound;
  };
  this.getSound = function() {
        return this.userData.sound;
  };

  this.setStatus = function(status) {
        this.userData.status = status;
  };
  this.getStatus = function() {
        return this.userData.status;
  };

  this.setPeriod = function(id_period) {
        this.userData.id_period = id_period;
  };
  this.getPeriod = function() {
        return this.userData.id_period;
  };

})

.service('professorService', function() {
  this.professorData = {};

  this.professor = function() {
        return this.professorData;
  };

  this.setId = function(id) {
        this.professorData.id = id;
  };
  this.getId = function() {
        return this.professorData.id;
  };

  this.setFirstname = function(first_name) {
        this.professorData.first_name = first_name;
  };
  this.getFirstname = function() {
        return this.professorData.first_name;
  };

  this.setLastname = function(last_name) {
        this.professorData.last_name = last_name;
  };
  this.getLastname = function() {
        return this.professorData.last_name;
  };

  this.setAddress = function(address) {
        this.professorData.address = address;
  };
  this.getAddress = function() {
        return this.professorData.address;
  };

  this.setCountry = function(country) {
        this.professorData.country = country;
  };
  this.getCountry = function() {
        return this.professorData.country;
  };

  this.setState = function(state) {
        this.professorData.state = state;
  };
  this.getState = function() {
        return this.professorData.state;
  };

  this.setCity = function(city) {
        this.professorData.city = city;
  };
  this.getCity = function() {
        return this.professorData.city;
  };

  this.setPostalCode = function(postal_code) {
        this.professorData.postal_code = postal_code;
  };
  this.getPostalCode = function() {
        return this.professorData.postal_code;
  };

})

.service('studentService', function() {
  this.studentData = {};

  this.student = function() {
        return this.professorData;
  };

  this.setId = function(id) {
        this.studentData.id = id;
  };
  this.getId = function() {
        return this.studentData.id;
  };

  this.setFirstname = function(first_name) {
        this.studentData.first_name = first_name;
  };
  this.getFirstname = function() {
        return this.studentData.first_name;
  };

  this.setLastname = function(last_name) {
        this.studentData.last_name = last_name;
  };
  this.getLastname = function() {
        return this.studentData.last_name;
  };

  this.setEmail = function(email) {
        this.studentData.email = email;
  };
  this.getEmail = function() {
        return this.studentData.email;
  };

  this.setAddress = function(address) {
        this.studentData.address = address;
  };
  this.getAddress = function() {
        return this.studentData.address;
  };

  this.setCountry = function(country) {
        this.studentData.country = country;
  };
  this.getCountry = function() {
        return this.studentData.country;
  };

  this.setState = function(state) {
        this.studentData.state = state;
  };
  this.getState = function() {
        return this.studentData.state;
  };

  this.setCity = function(city) {
        this.studentData.city = city;
  };
  this.getCity = function() {
        return this.studentData.city;
  };

  this.setPostalCode = function(postal_code) {
        this.studentData.postal_code = postal_code;
  };
  this.getPostalCode = function() {
        return this.studentData.postal_code;
  };

})

// ------------------ PROVIDERS -------------------
 .provider('mainView', function() {

 this.getCurrentMainView = function() {
   var exp = window.localStorage.getItem('count');

   if (exp == 1) {
     return "/app/courses";
   }
   else {
     return "/app/login";
   }
 };
 this.$get = ["nullService", function(){ return null; }];
})
