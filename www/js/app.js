angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','jrCrop','ngCordova'])
// Para mejorar el rendimiento cuando está en producción.
.config(['$compileProvider', function ($compileProvider) {
  if (window.cordova) {
    $compileProvider.debugInfoEnabled(false);
  }
}])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, mainViewProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.login', {
    cache: false,
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })
  .state('app.forgotPassword', {
      url: '/forgotPassword',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgotPassword.html',
          controller: 'forgotPasswordCtrl'
        }
      }
  })
  .state('app.changePassword', {
      url: '/changePassword',
      views: {
        'menuContent': {
          templateUrl: 'templates/changePassword.html',
          controller: 'changePasswordCtrl'
        }
      }
  })
  .state('app.courses', {
    cache: false,
    reload: true,
    url: '/courses',
    views: {
      'menuContent': {
        templateUrl: 'templates/courses.html',
        controller: 'CoursesCtrl'
      }
    }
  })
  .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })
  .state('app.about', {
    url: '/about',
    views: {
      'menuContent': {
        templateUrl: 'templates/about.html'
      }
    }
  })
  .state('app.course', {
    url: '/courses/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/course.html',
        controller: 'CourseCtrl'
      }
    }
  });

  //$urlRouterProvider.otherwise('/login');
  $urlRouterProvider.otherwise(mainViewProvider.getCurrentMainView());
});
