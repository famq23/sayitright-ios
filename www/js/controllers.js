angular.module('starter.controllers', ['jrCrop','ngCordova'] )
.controller('AppCtrl', function($scope, $timeout, $http) {

})

.controller('LoginCtrl', function($scope, $rootScope, $timeout, $http, userService, $state, $location) {
  $scope.user = {};
  $scope.loginData = {};
  $scope.check = {};
  $scope.check.val = false;

  // Login
  $scope.doLogin = function() {

  if($scope.check.val == true){

     var headers = {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'};

     $http.post('http://www.diazapps.com/sayitright_ios/login.php',{
       user_name: $scope.loginData.username,
       user_pass: $scope.loginData.password
     },{
     headers : headers
     }).then(function(response){
        $scope.id = response.data.id_user;
        if(angular.isString($scope.id) === true){
           userService.setId(response.data.id_user);
           userService.setEmail(response.data.email);
           userService.setIdRol(response.data.id_rol);
           userService.setPicture(response.data.picture);
           userService.setSound(response.data.sound);
           userService.setStatus(response.data.status);
           userService.setFirstname(response.data.first_name);
           userService.setLastname(response.data.last_name);
           userService.setPeriod(response.data.id_period);

           // Agignar el localStorage después de hacer el Login.
           window.localStorage.setItem('count', 1);
           window.localStorage.setItem('nameProfile', userService.userData.first_name);
           window.localStorage.setItem('lastnameProfile', userService.userData.last_name);
           window.localStorage.setItem('emailProfile', userService.userData.email);
           window.localStorage.setItem('imageProfile', userService.userData.picture);
           window.localStorage.setItem('soundProfile', userService.userData.sound);
           window.localStorage.setItem('idProfile', userService.userData.id);
           window.localStorage.setItem('idRol', userService.userData.id_rol);
           window.localStorage.setItem('idPeriod', userService.userData.id_period);

           // Para cargar datos del perfil en el Menú Toogle Left
           var firstName = window.localStorage.getItem('nameProfile');
           var pictureProfile = window.localStorage.getItem('imageProfile');
           $rootScope.nameProfile = firstName;
           $rootScope.pictureProfile = pictureProfile;
           $state.go('app.courses');
        } else{
          alert('Incorrect Credentials. Please try again.');
        }
    },function errorCallback(response) {
      alert(response);
    })
  } else{
      alert('Please accept the terms and conditions');
  }

} // fin doLogin

})

/* --- Logout Button Ctrl --- */
.controller('logoutCtrl', function($ionicPlatform, $scope, $location) {
  $scope.logOut = function() {
    $location.path("/app/login");
    // Destruir los datos del localStorage.
    window.localStorage.removeItem('count');
    window.localStorage.removeItem('nameProfile');
    window.localStorage.removeItem('lastnameProfile');
    window.localStorage.removeItem('emailProfile');
    window.localStorage.removeItem('imageProfile');
    window.localStorage.removeItem('soundProfile');
    window.localStorage.removeItem('idProfile');
    window.localStorage.removeItem('idRol');
    window.localStorage.removeItem('idPeriod');
  }
})

/* --- Inicio Olvidó Clave Ctrl --- */
.controller('forgotPasswordCtrl', function($scope, $ionicModal, $ionicPopup, $timeout, $http, $state, $location) {
  $scope.data = {};
  $scope.response = [];

  $scope.doForgotPassword = function() {
    var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
    var link = 'http://www.diazapps.com/sayitright_ios/reset_user_password.php';

    $http.post(link,{
      email: $scope.data.email,
    },{
      headers: headers
    }).then(function successCallback(response){
      $scope.response = response.data.message;
      $scope.status = response.data.status;
      //console.log('Response Mensaje: ' + $scope.response);
      //console.log('Response Status: ' + $scope.status);

      if ($scope.status == 'success') {
        $scope.enviado = $ionicPopup.show({
          title: $scope.response
        });
        setTimeout(function() {
          $scope.enviado.close();
          $location.path("/app/login");
        }, 4000);
      }else{
        $scope.cancelado = $ionicPopup.show({
          title: $scope.response
        });
        setTimeout(function() {
          $scope.cancelado.close();
        }, 4000);
      }
    }, function errorCallback(response) {
       alert('Error, can not connect to server. Try again.');
    })
  }
})

/* --- Inicio Cambiar Clave Ctrl --- */
.controller('changePasswordCtrl', function($scope, $ionicModal, userService, $ionicPopup, $timeout, $http, $location) {
  $scope.data = {};
  $scope.response = [];

  $scope.Username = window.localStorage.getItem('emailProfile');

  $scope.doChangePassword = function() {
    var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
    var link = 'http://www.diazapps.com/sayitright_ios/update_user_password.php';

    if ($scope.data.newpassword != $scope.data.newpassword2) {
      alert('The field New Password and Retype Password it does not match, please check fine.');
    }else{

      $http.post(link,{
        username: userService.userData.id,
        password: $scope.data.password,
        newpassword: $scope.data.newpassword,
      },{
        headers: headers
      }).then(function successCallback(response){
        $scope.response = response.data.message;
        $scope.status = response.data.status;
        //console.log('Response Mensaje: ' + $scope.response);
        //console.log('Response Status: ' + $scope.status);

        if ($scope.status == 'success') {
          $scope.cambioClave = $ionicPopup.show({
            title: $scope.response
          });
          setTimeout(function() {
            $scope.cambioClave.close();
            $location.path("/app/login");
            // Destruir los datos del localStorage al redireccionar al login.
            window.localStorage.removeItem('count');
            window.localStorage.removeItem('nameProfile');
            window.localStorage.removeItem('lastnameProfile');
            window.localStorage.removeItem('emailProfile');
            window.localStorage.removeItem('imageProfile');
            window.localStorage.removeItem('soundProfile');
            window.localStorage.removeItem('idProfile');
            window.localStorage.removeItem('idRol');
            window.localStorage.removeItem('idPeriod');
          }, 4000);
        }else{
          $scope.errorCambioClave = $ionicPopup.show({
            title: $scope.response
          });
          setTimeout(function() {
            $scope.errorCambioClave.close();
          }, 4000);
        }

      }, function errorCallback(response) {
         alert('Error, can not connect to server. Try again.');
      })
    }// fin else
  }
})
/* --- Fin Cambiar Clave Ctrl --- */

.controller('CoursesCtrl', function($scope, $rootScope, $http, userService, professorService, studentService) {
 $scope.id_courses = [];
 $scope.courses_name = [];
 $scope.id_sections = [];
 $scope.name_student = [];
 $scope.sections = [];
 $scope.sections_name = [];

 // Obtener el localStorage para mantener la sesión.
 var exp1 = window.localStorage.getItem('count');
 var exp2 = window.localStorage.getItem('nameProfile');
 var exp3 = window.localStorage.getItem('lastnameProfile');
 var exp4 = window.localStorage.getItem('emailProfile');
 var exp5 = window.localStorage.getItem('imageProfile');
 var exp6 = window.localStorage.getItem('soundProfile');
 var exp7 = window.localStorage.getItem('idProfile');
 var exp8 = window.localStorage.getItem('idRol');
 var exp9 = window.localStorage.getItem('idPeriod');

 $rootScope.nameProfile = exp2;
 $rootScope.pictureProfile = exp5;

 // Para actualizar datos en el userService para el localStorage y evita que
 // perder los datos al recargar la app, manteniendo la sesión.
 userService.setId(exp7);
 userService.setIdRol(exp8);
 userService.setFirstname(exp2);
 userService.setLastname(exp3);
 userService.setPicture(exp5);
 userService.setSound(exp6);
 userService.setPeriod(exp9);

 $scope.id_rol_user = userService.userData.id_rol; // Para la vista courses.html

 var headers = {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'};
 //console.log('Id Usuario: ' + userService.userData.id + ' | id rol: ' + userService.userData.id_rol + ' | Periodo: ' + userService.userData.id_period);

 if(userService.userData.id_rol == 3){
 //Get Student
 $http.post('http://www.diazapps.com/sayitright_ios/get_user_info_angular.php',{
   id_user: userService.userData.id,
   id_rol: 3
 },{
    headers : headers
  }).then(function(response2){
  //console.log('Id estudiante: ' + response2.data.id_student);
  //console.log('Email estudiante: ' + response2.data.email);
  $scope.id_student = response2.data.id_student;
  $scope.email_student = response2.data.email;
  studentService.setId(response2.data.id_student);
  studentService.setFirstname(response2.data.first_name);
  studentService.setLastname(response2.data.last_name);
  studentService.setEmail(response2.data.email);
  studentService.setAddress(response2.data.address);
  studentService.setCountry(response2.data.country);
  studentService.setState(response2.data.state);
  studentService.setCity(response2.data.city);
  studentService.setPostalCode(response2.data.postal_code);

  //Get Enrollment
  $http.post('http://www.diazapps.com/sayitright_ios/get_enrolled_students_angular_student.php',{
    id_student: $scope.id_student
  },{
    headers: headers
    }).then(function(response3){
      ////console.log('Secciones: ' + JSON.stringify(response3.data.students));
      angular.forEach(response3.data.students, function(student){
        //$scope.name_student.push(student.first_name);
        $scope.id_sections.push(student.id_section);
      });
      //console.log('Nombre estudiante: ' + $scope.name_student);
      //console.log('Id Section: ' + $scope.id_sections);


      //Get Section
      $http.post('http://www.diazapps.com/sayitright_ios/get_section_angular.php',{
        id_sections: $scope.id_sections,
        id_period: userService.userData.id_period
      },{
          headers: headers
        }).then(function(response4){
           // la variable 'sections' se pasa a la vista courses para mostrar los datos
           $scope.sections = response4.data.sections;
           /*
           console.log('Secciones estudiante: ' + JSON.stringify(response4.data.sections));
           console.log('Nombre sección: ' + response4.data.name);
           $scope.period = userService.userData.id_period;
           angular.forEach(response4.data.sections, function(section){
             $scope.id_courses.push(section.id_course);
           });
           console.log('Periodo: ' + $scope.period);
           console.log('Estos son los id de los cursos: ' + $scope.id_courses)
           */

           /*
           //Get courses-name student
           $http.post('http://www.diazapps.com/sayitright_ios/get_course_names_angular.php',{
             id_courses: $scope.id_courses
           },{
               headers: headers
             }).then(function(response19){
               console.log('Nombre cursos del estudiante: ' + JSON.stringify(response19.data.courses));
               $scope.courses_name = response19.data.courses;
           })
           */
          },function errorCallback(response4) {
            alert(response)
          })
},function errorCallback(response3) {
    alert(response)
  })
},function errorCallback(response2) {
    alert(response)
  })

}; // fin if id_rol 3 - estudiante

if(userService.userData.id_rol == 2){
//Get Professor
$http.post('http://www.diazapps.com/sayitright_ios/get_user_info_angular.php',{
  id_user: userService.userData.id,
  id_rol: 2
},{
    headers : headers
  }).then(function(response2){

  $scope.id_professor= response2.data.id_professor;
  professorService.setId(response2.data.id_professor);
  professorService.setFirstname(response2.data.first_name);
  professorService.setLastname(response2.data.last_name);
  professorService.setAddress(response2.data.address);
  professorService.setCountry(response2.data.country);
  professorService.setState(response2.data.state);
  professorService.setCity(response2.data.city);
  professorService.setPostalCode(response2.data.postal_code);


//Get Instructor
$http.post('http://www.diazapps.com/sayitright_ios/get_instructor_angular.php',{
  id_professor: $scope.id_professor
},{
    headers : headers
    }).then(function(response3){
         //console.log('Instructor: ' + JSON.stringify(response3.data.instructors));
         angular.forEach(response3.data.instructors, function(instructor){
           $scope.id_sections.push(instructor.id_section);
         });
         //console.log('Id Secciones: ' + $scope.id_sections);

    //Get Section
    $http.post('http://www.diazapps.com/sayitright_ios/get_section_angular_prof.php',{
      id_sections: $scope.id_sections,
      id_period: userService.userData.id_period
    },{
        headers : headers
        }).then(function(response4){
          // la variable 'section' se pasa a la vista courses para mostrar los datos
          $scope.sections = response4.data.sections;
          /*
          console.log('Secciones: ' + JSON.stringify(response4.data.sections));
          angular.forEach(response4.data.sections, function(sections){
            $scope.id_courses.push(sections.id_course);
            $scope.sections_name.push(sections.name_section);
            $scope.courses_name.push(sections.name_course);
          });
          console.log('Id cursos: ' + $scope.id_courses);
          console.log('Nombre secciones: ' + $scope.sections_name);
          console.log('Nombre cursos: ' + $scope.courses_name);
          */
          //$scope.section = response4.data.sections;

          /*
          //Get Courses name
          $http.post('http://www.diazapps.com/sayitright_ios/get_course_names_angular.php',{
            id_courses: $scope.id_courses
          },{
              headers : headers
              }).then(function(response9){
              console.log('Nombre cursos: ' + JSON.stringify(response9.data.courses));
              $scope.courses_name = response9.data.courses;
          })
          */
          },function errorCallback(response4) {
              alert(response4.data)
            })
      },function errorCallback(response3) {
          alert(response3.data)
        })

},function errorCallback(response2) {
    alert(response2.data)
  })
 };

})

.filter('diazapps', function($sce) {
    return function(sound) {
      return $sce.trustAsResourceUrl('http://www.diazapps.com/' + sound);
    };
})

.controller('CourseCtrl', function($scope, $stateParams, $http, $sce) {
 $scope.playAudio = function(index) {
    var audio = document.getElementById("audio_play"+index);
    audio.load();
    audio.play();
    // var underline1 = index;
    // var underline2 = index;
    // console.log('Ese: ' + underline1);
    // $scope.underline1 = { 'color': '#3a37e4' };
    // $scope.underline2 = { 'color': '#3a37e4' };
 };

  //Get Section Professor
var headers = {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'};
$http.post('http://www.diazapps.com/sayitright_ios/get_section_professor_angular.php',{
  id_section: $stateParams.id
},{
    headers : headers
    }).then(function(response5){
     $scope.professor = response5.data;
     //console.log('Sección por profesor: ' + JSON.stringify($scope.professor));

     $scope.trustSrc = function(src) {
       return $sce.trustAsResourceUrl(src);
     }
     //console.log('TrustSRC: ' + $scope.trustSrc);
})

  //Get Section Teaching Assistant
var headers = {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'};
$http.post('http://www.diazapps.com/sayitright_ios/get_section_teaching_assistant_angular.php',{
  id_section: $stateParams.id
},{
    headers : headers
  }).then(function(response6){
     $scope.assistant = response6.data;
     //console.log('Asistente: ' + JSON.stringify($scope.assistant));
})

  //Get Section Enrolled Students
 var headers = {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'};
 $http.post('http://www.diazapps.com/sayitright_ios/get_enrolled_students_angular.php',{
  id_section: $stateParams.id
 },{
      headers : headers
    }).then(function(response7){
     $scope.students = response7.data.students;
     //console.log('Estudiantes: ' + JSON.stringify($scope.students));
})

$http.post('http://www.diazapps.com/sayitright_ios/get_one_section_angular.php',{
  id_sections: $stateParams.id
},{
    headers : headers
    }).then(function(response8){
    $scope.section = response8.data.sections[0];
    //console.log('Section desde course: ' + $scope.section.name);

    $http.post('http://www.diazapps.com/sayitright_ios/get_course_name_angular.php',{
    id_courses: $scope.section.id_course
    },{
      headers : headers
    }).then(function(response9){
      $scope.course = response9.data.sections[0];
      //console.log('Curso seleccionado: ' + $scope.course.name);
   })

   })
})


.controller('SettingsCtrl', function($ionicPlatform, userService, $rootScope, $scope, $cordovaMedia, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $ionicPopup, $filter, $timeout, $http, $location, $state, $jrCrop) {
  $scope.data = {};
  $scope.user = userService.userData;

  $ionicPlatform.ready(function() {
    $scope.imgURI = null;
    $scope.name_sound_path = '';

    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };
    $scope.playing = function(src){
        if (src != null){
            $scope.play(src);
        }else{
            //console.log('No hay sonido!');
        }
    };

    $scope.recordStart = function(){
        $scope.tempSrc = null;
        var random = 0;
        random = Math.round(Math.random()*1000000);
        var fecha = $filter('date')(new Date(),'ddMMyyyy');

        $scope.tempSrc = "documents://name_profile_"+fecha+random+"_ios.m4a";

        var media = new Media($scope.tempSrc);
        // Record MPEG compressed audio, single channel at 16kHz
        var options = {
            SampleRate: 16000,
            NumberOfChannels: 1
        }
        media.startRecordWithCompression(options);

        $scope.showPopup();
        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };
        $scope.name_sound_path = $scope.tempSrc;
    };

    $scope.showPopup = function() {
      $scope.alertPopup = $ionicPopup.show({
        title: 'Recording name audio...'
      });
    };

    $scope.savedPopup = function() {
       $scope.saved = $ionicPopup.show({
         title: '¡ The profile is updated !',
         buttons: [
                      { text: 'Ok',
                        type: 'button-dark',
                        onTap: function() {
                             $state.go('app.courses');
                        }
                      }
                  ]
       });
    };

  }); // Fin ionicPlatform.ready

  $scope.gallery = function() {
      $scope.imgURI = null;
      var options = {
          quality : 75,
          destinationType : Camera.DestinationType.FILE_URI,
          sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
          allowEdit : false,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 600,
          targetHeight: 600,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
          var rutaImagen = imageData;
          $scope.crop(rutaImagen);

      }, function(err) {
      });
  }

  $scope.takePicture = function() {
      $scope.imgURI = null;
      var options = {
          quality : 75,
          destinationType : Camera.DestinationType.FILE_URI,
          sourceType : Camera.PictureSourceType.CAMERA,
          allowEdit : false,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 600,
          targetHeight: 600,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
        var rutaImagen = imageData;
        $scope.crop(rutaImagen);

      }, function(err) {
      });
  }

  // Función que convierte la imagen base64 en Blob, antes de guardar la imagen como archivo.
  function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
  }

  // Función que guarda la imagen como archivo desde base64
  function savebase64AsImageFile(folderpath,filename,content,contentType){
    // Convert the base64 string in a Blob
    var DataBlob = b64toBlob(content,contentType);
    window.resolveLocalFileSystemURL(folderpath, function(dir) {
		    dir.getFile(filename, {create:true}, function(file) {
            //console.log("File created succesfully.");
            file.createWriter(function(fileWriter) {
                //console.log("Writing content to file.");
                fileWriter.write(DataBlob);
            }, function(){
                //console.log('Error writing file.');
            });
		});
    });
  }

  $scope.crop = function(rutaImagen) {
      $jrCrop.crop({
          url: rutaImagen,
          width: 360,
          height: 352,
          title: 'Move and scale'
      }).then(function(canvas) {
          var image = canvas.toDataURL('image/jpeg',0.80);
          // Resultado imagen en base64
          $scope.imgURI = image;
          $scope.rutaImagenPerfil = $scope.imgURI; // Para la vista en Settings

          // Para convertir y guardar la imagen como archivo en jpg desde la base64.
          var myBaseString = $scope.imgURI;
          var block = myBaseString.split(";");
          var dataType = block[0].split(":")[1]; // Extrae "image/jpeg"
          var realData = block[1].split(",")[1]; // Extrae "iVBORw0KGg..."
          var folderpath = cordova.file.tempDirectory;
          var dateNow = new Date(),
          now = dateNow.getTime(),
          filename =  "image_profile_" + now + ".jpg"; // The name of the file in jpeg for create.
          savebase64AsImageFile(folderpath,filename,realData,dataType);
          $scope.imgURI = filename;

          // Retorna el valor de la ruta nueva para el cordovaFileTransfer.upload
          $scope.pathForImage = function(imgURI) {
            if (imgURI == null) {
              return '';
            } else {
              return cordova.file.tempDirectory + imgURI;
            }
          };

      }, function() {
      });
  } // Fin Crop.

  $scope.addMedia = function() {
    $scope.edit = $ionicPopup.show({
      title: 'Select image from:',
      cssClass: 'popup-vertical-buttons',
      buttons: [
              { text: 'Gallery',
                type: 'button-light button-full',
                onTap: function() {
                     $scope.gallery();
                }
              },
              { text: 'Camera',
                type: 'button-light button-full',
                onTap: function() {
                     $scope.takePicture();
                }
              },
              { text: 'Cancel',
                type: 'button-light button-full',
                onTap: function() {
                     return
                }
              }
            ]
      });
  }

  // Subir foto del Perfil al servidor.
  $scope.subirImagenAlServidor = function(){
    var url = "http://www.diazapps.com/sayitright_ios/upload.php";
    var targetPath = $scope.pathForImage($scope.imgURI);
    var filename = targetPath.split("/").pop();
    var options = {
       fileKey: "fileToUpload",
       fileName: filename,
       chunkedMode: true,
       mimeType: "image/jpeg",
       httpMethod: "POST",
       params: {
         'directory': 'pic_upload',
         'filename': filename,
         'id_user': userService.userData.id
       }
   };

   $cordovaFileTransfer.upload(url, targetPath, options, true).then(function(result) {
      //console.log("Subió la foto al servidor !");
      //console.log('Respuesta foto desde servidor: ' + JSON.stringify(result));
   }, function(err) {
      alert('Error uploading the picture.')
   }, function (progress) {

   });
  } // Fin subir foto

  // Subir audio del perfil al servidor.
  $scope.subirAudioAlServidor = function(){
     var url = "http://www.diazapps.com/sayitright_ios/upload.php";

     if ($scope.tempSrc != null) {
         var targetPathAudio1 = $scope.tempSrc;
         var filenameAudio1 = targetPathAudio1.split("/").pop();
         var targetPathAudioNew1 = cordova.file.documentsDirectory + filenameAudio1;
         var options = {
            fileKey: "fileToUpload",
            fileName: filenameAudio1,
            chunkedMode: false,
            mimeType: "audio/m4a",
            httpMethod: "POST",
            params: {
              'directory': 'sound_rec',
              'filename': filenameAudio1,
              'id_user': userService.userData.id
            }
         };

        $cordovaFileTransfer.upload(url, targetPathAudioNew1, options, true).then(function(result) {
           //console.log("Subió el audio al servidor !");
           //console.log('Respuesta audio desde servidor con result: ' + JSON.stringify(result));
        }, function(err) {
           //console.log("ERROR: " + JSON.stringify(err));
        }, function (progress) {

        });
     }
   } // fin subir nombre del perfil audio

   $scope.updateProfile = function(){
       var src = null;

       var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
       var link = 'http://www.diazapps.com/sayitright_ios/update_user.php';

       // Subir imagen al servidor
       if ($scope.imgURI != null) {
         $scope.subirImagenAlServidor();
       }
       // Subir Audio al servidor
       if ($scope.tempSrc != null) {
         $scope.subirAudioAlServidor();
       }

       $http.post(link,{
         id_rol: userService.userData.id_rol,
         first_name: $scope.user.first_name,
         last_name: $scope.user.last_name,
         id_user: userService.userData.id
       },{
         headers: headers
       }).then(function(response){
         //console.log('Perfil actualizado en la base de datos de SayMyNameRight !');
         //console.log('Response: ' + JSON.stringify(response));
         $scope.savedPopup();

         // Para actualizar datos en el userService
         userService.setFirstname(response.data.first_name);
         userService.setLastname(response.data.last_name);
         userService.setPicture(response.data.picture);
         userService.setSound(response.data.sound);

         // Para asignar datos al localStorage después de actualizar el perfil.
         window.localStorage.removeItem('nameProfile');
         window.localStorage.removeItem('lastnameProfile');
         window.localStorage.removeItem('imageProfile');
         window.localStorage.removeItem('soundProfile');
         window.localStorage.setItem('nameProfile',response.data.first_name);
         window.localStorage.setItem('lastnameProfile',response.data.last_name);
         window.localStorage.setItem('imageProfile',response.data.picture);
         window.localStorage.setItem('soundProfile',response.data.sound);

       },function errorCallback(response) {
         //console.log(JSON.stringify(response));
         alert('__Error__');
       })

   }; // fin updateProfile

});
